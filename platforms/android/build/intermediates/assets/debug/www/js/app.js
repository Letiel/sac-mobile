angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCordova', 'angular-md5'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html'
  })

  .state('tab.dadosGerais', {
    url: '/dadosGerais',
    views: {
      'dados_gerais': {
        templateUrl: 'templates/dados_gerais.html',
        controller: 'dadosGerais_controller'
      }
    }
  })

  .state('tab.pesquisa', {
    url: '/pesquisa',
    views: {
      'pesquisa': {
        templateUrl: 'templates/pesquisa.html',
        controller: 'pesquisa_controller'
      }
    }
  })

  .state('tab.resultado', {
    url: '/resultado/:id_resultado',
    views: {
      'pesquisa': {
        templateUrl: 'templates/resultado_pesquisa.html',
        controller: 'resultado_controller'
      }
    }
  })

  .state('tab.resultado_pesquisa', {
    url: '/resultado_pesquisa',
    views: {
      'pesquisa': {
        templateUrl: 'templates/pesquisa.html',
        controller: 'resultado_pesquisa_controller'
      }
    }
  })

  .state('tab.mapa', {
    url: '/mapa/:lat/:lng/:nome',
    views: {
      'pesquisa': {
        templateUrl: 'templates/mapa.html',
        controller: 'mapa_controller'
      }
    }
  })

  .state('tab.historico', {
    url: '/historico/:id',
    views: {
      'pesquisa': {
        templateUrl: 'templates/historico.html',
        controller: 'historico_controller'
      }
    }
  });

  $urlRouterProvider.otherwise('login');

});
