angular.module('starter.controllers', [])

.controller('login_controller', function ($scope, $state, $http){
  $scope.submit = function(){
    login = $scope.login;
    senha = $scope.senha;
    if(login != null && senha != null && login != "" && senha != ""){
      $scope.loading = true;
      $http.get('http://www.precisionsistemas.com.br/API/login/'+login+"/"+senha).success(function (data) {
          $scope.loading = false;
          if(data.achou == 's'){
            window.localStorage.setItem('token', data.token);
            $state.go("tab.dadosGerais");
          } else {
            alert("Usuário ou senha inválidos");
          }
        });
    } else {
      alert("Preencha os campos.");
    }
  };
})

.controller('dadosGerais_controller', function($scope, $stateParams, $http, $state) {
  $scope.loading = true;
  token = window.localStorage.getItem('token');
  if(token == null || token == ""){
    $state.go("login");
    exit();
  }
  $http.get('http://www.precisionsistemas.com.br/API/dados_gerais?token='+token).success(function (data) {
    if(data.token == token){
      $scope.dados = data;
      $scope.loading = false;
    } else {
      $state.go("login");
    }
  });
})

.controller('pesquisa_controller', function($scope, $stateParams, $http) {})

.controller('resultado_pesquisa_controller', function($scope, $stateParams, $http) {
  $scope.submit = function(){
    token = window.localStorage.getItem('token');
    if(token == null || token == ""){
      $state.go("login");
      exit();
    }
    $scope.loading = true;
    $http.post('http://www.precisionsistemas.com.br/API/pesquisa/'+$scope.pesquisa+"?token="+token).success(function (data) {
      $scope.resultados = data;
      $scope.loading = false;
    });
  };
})

.controller('mapa_controller', function($scope, $stateParams, $cordovaGeolocation) {
    $scope.latitude = $stateParams.lat;
    $scope.longitude = $stateParams.lng;

    console.log($stateParams);
    // var options = {timeout: 10000, enableHighAccuracy: true};

      // $cordovaGeolocation.getCurrentPosition(options).then(function(position){

        var latLng = new google.maps.LatLng($stateParams.lat, $stateParams.lng);

        var mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };


        $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng($stateParams.lat, $stateParams.lng),
            title: $stateParams.nome,
            map: $scope.map
        });
      // }, function(error){
      //   alert("Não foi possível carregar o mapa.");
      // });
})

.controller('resultado_controller', function($scope, $stateParams, $http) {
      token = window.localStorage.getItem('token');
      if(token == null || token == ""){
        $state.go("login");
        exit();
      }
      $scope.loading = true;
      $http.post('http://www.precisionsistemas.com.br/API/resultado_pesquisa/'+$stateParams.id_resultado+"?token="+token).success(function (data) {
        $scope.resultado = data;
        $scope.token = window.localStorage.getItem('token');
        $scope.loading = false;
      }).error(function(){
        $scope.loading = false;
        alert("Erro ao consultar");
      });
})

.controller('historico_controller', function($scope, $stateParams, $http) {
    id = $stateParams.id;
    $scope.loading = true;
    token = window.localStorage.getItem('token');
    if(token == null || token == ""){
      $state.go("login");
      exit();
    }
    // $scope.loading = false;
    // $scope.resultado = 'http://www.precisionsistemas.com.br/API/historico/'+id+'?token='+token;

    $http.get('http://www.precisionsistemas.com.br/API/historico/'+id+'?token='+token).success(function (data) {
      if(data.token == token){
        $scope.resultados = data;
        $scope.loading = false;
      } else {
        $scope.loading = false;
        $state.go("login");
      }
    }).error(function(){
      $scope.loading = false;
      alert("Erro ao consultar");
    });
});
